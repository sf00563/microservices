from flask import Flask
import flask_nicely
from werkzeug.exceptions import NotFound
import json
import os

app = Flask(__name__)
with open("{}/database/movies.json".format(os.getcwd()), "r") as f:
    movies = json.load(f)


@app.route("/", methods=['GET'])
@flask_nicely.nice_json
def hello():
    data = {
        "uri": "/",
        "subresource_uris": {
            "movies": "/movies",
            "movie": "/movies/<id>"
        }
    }
    return data

@app.route("/movies/<movieid>", methods=['GET'])
@flask_nicely.nice_json
def movie_info(movieid):
    if movieid not in movies:
        raise NotFound

    result = movies[movieid]
    result["uri"] = "/movies/{}".format(movieid)

    return result


@app.route("/movies", methods=['GET'])
@flask_nicely.nice_json
def movie_record():
    return movies


if __name__ == "__main__":
    app.run(port=5001, debug=True)

