from flask import Flask
import flask_nicely
from werkzeug.exceptions import NotFound
import json
import os


app = Flask(__name__)

with open("{}/database/showtimes.json".format(os.getcwd()), "r") as f:
    showtimes = json.load(f)


@app.route("/", methods=['GET'])
@flask_nicely.nice_json
def hello():
    
    data = {
        "uri": "/",
        "subresource_uris": {
            "showtimes": "/showtimes",
            "showtime": "/showtimes/<date>"
        }
    }
    return data


@app.route("/showtimes", methods=['GET'])
@flask_nicely.nice_json
def showtimes_list():
    return showtimes


@app.route("/showtimes/<date>", methods=['GET'])
@flask_nicely.nice_json
def showtimes_record(date):
    if date not in showtimes:
        raise NotFound
    print(showtimes[date])
    return showtimes[date]

if __name__ == "__main__":
    app.run(port=5002, debug=True)
